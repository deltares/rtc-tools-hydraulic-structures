Python API
==========

Pumping Station Mixin
---------------------

.. autoclass:: rtctools_hydraulic_structures.pumping_station_mixin.PumpingStationMixin
    :members: pumpingstation_cache_hq_subproblem, pumpingstation_energy_price_symbol, pumping_stations
    :show-inheritance:

.. automodule:: rtctools_hydraulic_structures.pumping_station_mixin
    :members: Pump, Resistance, PumpingStation, MinimizePumpCostGoal, MinimizePumpEnergyGoal, plot_operating_points
    :special-members: __init__, __getattr__
    :show-inheritance:

Weir Mixin
----------

.. autoclass:: rtctools_hydraulic_structures.weir_mixin.WeirMixin
    :members: weirs
    :show-inheritance:

.. automodule:: rtctools_hydraulic_structures.weir_mixin
    :members: Weir
    :show-inheritance:
