.. RTC-Tools Hydraulic Structures documentation master file, created by
   sphinx-quickstart on Wed Jul 12 19:20:46 2017.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to RTC-Tools Hydraulic Structures's documentation!
==========================================================

Contents:
=========

.. toctree::
   :maxdepth: 2
   :caption: User Documentation

   getting-started
   support

.. toctree::
   :maxdepth: 2
   :caption: API Documentation

   python-api
   modelica-api

.. toctree::
   :maxdepth: 2
   :caption: Examples

   examples/index

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
